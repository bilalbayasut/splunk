import logging
from pprint import pprint

import click
import splunklib.client as client


@click.command()
@click.option("--url", required=True, help="The URL of the Splunk instance.")
@click.option(
    "--username", required=True, help="The username to authenticate with Splunk."
)
@click.option(
    "--password", required=True, help="The password to authenticate with Splunk."
)
@click.option(
    "--index",
    required=True,
    help="The name of the Splunk index to send the log data to.",
)
@click.option("--log-file", required=True, help="The path to the log file to parse.")
def send_to_splunk(url, username, password, index, log_file):
    # Set up logging
    logging.basicConfig(filename="send_to_splunk.log", level=logging.DEBUG)

    def _parse_details(input_str):
        output_dict = {}
        pairs = input_str.split(";")
        for pair in pairs:
            key, value = pair.split("=")
            output_dict[key] = value
        return output_dict

    def _parse_input(input_str):
        output_dict = {}
        pairs = input_str.split("\n")[:-1]
        for pair in pairs:
            key, value = pair.split("=", 1)
            len_split = len(pair.split("="))
            if len_split > 2:
                output_dict[key] = _parse_details(value)
            else:
                output_dict[key] = value
        return output_dict

    try:
        # Connect to the Splunk instance
        params = {
            "host": url,
            "username": username,
            "password": password,
            "verify": False,
            "scheme": "https",
            "autologin": True,
            "port": 8089,
        }
        service = client.connect(**params)

        # Check if the index exists, if not create it
        if index not in [ix.name for ix in service.indexes.list()]:
            # Create the index if it does not exist
            service.indexes.create(index)
            logging.info(f"Index '{index}' created.")
        else:
            logging.info(f"Index '{index}' already exists.")

        # set the index
        index_obj = service.indexes[index]

        # Open the log file
        with open(log_file, "r") as f:
            for line in f:
                # Parse the log data
                log_parts = line.split("|")
                timestamp = log_parts[0]
                log_level = log_parts[1]
                log_message = log_parts[2]
                log_data = {}
                for item in log_parts[3:]:
                    item_keys = item.split("=")
                    # check if it's a key pair
                    if len(item_keys) == 2:
                        key, value = item.split("=")
                        log_data[key] = value.strip()

                    # check if it containes detailed log
                    if len(item_keys) > 2:
                        log_data = _parse_input(item)

                    pprint(log_data)
                    # Send the log data to Splunk
                    # index_obj.submit(
                    #     log_message, sourcetype=log_level, time=timestamp, **log_data
                    # )
                    # breakpoint()
                    # index_obj.submit(log_message, sourcetype=log_level, **log_data)
                    submit_param = {
                        "event": log_message,
                        # "host": url,
                        "source": log_file,
                        # "sourcetype": ""
                    }
                    index_obj.submit(**submit_param)

        # Disconnect from the Splunk instance
        service.logout()

        click.echo("Successfully sent log data to Splunk.")

    except Exception as e:
        # Log the error and exit with an error code
        logging.exception(e)
        click.echo(f"Failed to send log data to Splunk: {e}", err=True)
        raise SystemExit(1)


if __name__ == "__main__":
    send_to_splunk()
