# Splunk Installer
- there are 3 playbooks :-
- 1. test.playbook.yml : this is to ping the servers for debugging
- 2. install.playbook.yml : this playbook is for installing and starting splunk
- 3. playbook.yml: this playbook is to spin up vm

In order to run all the playbooks , execute :-

```
ansible-playbook main.playbook.yml -K
```
and fill in your sudo password accordingly.

if you want to test if you can access the IAP , run :-
```
gcloud compute ssh --zone=<zone> <instance_name> --tunnel-through-iap
```

# CLI APP
The cli app will parse from `sample.log` to splunk instance mentioned in cli param. :-
```
python cli.py --url <instance_dns_or_ip> --username <splunk_username> --password <splunk_password> --index <index_name> --log-file <log_file_path>
```
more detailed information :-

```
python cli.py --help
Usage: cli.py [OPTIONS]

Options:
  --url TEXT       The URL of the Splunk instance.  [required]
  --username TEXT  The username to authenticate with Splunk.  [required]
  --password TEXT  The password to authenticate with Splunk.  [required]
  --index TEXT     The name of the Splunk index to send the log data to.
                   [required]
  --log-file TEXT  The path to the log file to parse.  [required]
  --help           Show this message and exit.
```

## what is parse_test.py ?
It is just a python script to debug if the script can correctly prase sampe.log