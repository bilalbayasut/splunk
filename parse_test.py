from pprint import pprint


def parse_details(input_str):
    output_dict = {}
    pairs = input_str.split(";")
    for pair in pairs:
        key, value = pair.split("=")
        output_dict[key] = value
    return output_dict


def parse_input(input_str):
    output_dict = {}
    pairs = input_str.split("\n")[:-1]
    for pair in pairs:
        key, value = pair.split("=", 1)
        len_split = len(pair.split("="))
        if len_split > 2:
            # if key == "connection_string":
            output_dict[key] = parse_details(value)
        else:
            output_dict[key] = value
    return output_dict


# output_dict = parse_input(input_str)
# print(output_dict)

# Open the log file
with open("sample.log", "r") as f:
    for line in f:
        # Parse the log data
        log_parts = line.split("|")
        timestamp = log_parts[0]
        log_level = log_parts[1]
        log_message = log_parts[2]
        log_data = {}
        for item in log_parts[3:]:
            item_keys = item.split("=")
            # check if it's a key pair
            if len(item_keys) == 2:
                key, value = item.split("=")
                log_data[key] = value.strip()
                continue
            # check if it containes detailed log
            if len(item_keys) > 2:
                log_data = parse_input(item)
                continue
        pprint(log_data)
