# use new docker compose command if exist, else use deprecated one.
ifeq ($(which "docker compose"),)
    DOCKER_COMPOSE := docker-compose
else
    DOCKER_COMPOSE := docker compose
endif
DOCKER_COMPOSE_STACK=docker-compose.yml
DOCKER_COMPOSE_TESTS=docker-compose.tests.yml

K8S_STAGE_CLUSTER_NAME=pupil-cloud-staging-k8s

CERTS_PATH=./secrets/certs


export UID=$(id -u)
export GID=$(id -g)

help:  ## Display this help
	@awk 'BEGIN {FS = ":.*##"; printf "\nUsage:\n  make \033[36m<target>\033[0m\n"} /^[a-zA-Z_-]+:.*?##/ { printf "  \033[36m%-15s\033[0m %s\n", $$1, $$2 } /^##@/ { printf "\n\033[1m%s\033[0m\n", substr($$0, 5) } ' $(MAKEFILE_LIST)

##@ Setup

setup-env:
	@echo "                Creating .env file..."
	@echo "-----------------------------------------------------------"
	@[ ! -f .env ] && cp .env.example .env || echo ".env file already exists, not overwriting"


setup:
	@make setup-env
	@echo "-----------------------------------------------------------"
	@make setup-vscode
	@echo "-----------------------------------------------------------"
	@echo "                    Setup complete"
	@echo "-----------------------------------------------------------"
	@make setup-python
	@echo "-----------------------------------------------------------"

setup-vscode: ## Adds vscode settings
	@[ ! -d .vscode ] && cp -R .templates/vscode .vscode && echo ".vscode directory created" || echo ".vscode directory already exists, not overwriting"

setup-python: ## Create/update local python dev environment
	pyenv install -s
	@[ ! -f .venv ] && python -m venv .venv
	.venv/bin/pip install -U pip pipenv
	.venv/bin/pipenv install
